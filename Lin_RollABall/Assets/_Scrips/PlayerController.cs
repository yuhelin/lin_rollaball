﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    // scale player's speed
    public float speed;
    // counTtext reference
    public Text countText;
    // winText reference
    public Text winText;
    // pickUp prefab reference
    public GameObject pickupPrefab;
    // rigidbody for player
    private Rigidbody rb;
    // int for how many pick up the player has
    private int count;
    // boolean to check if the game is finished
    private bool gameFinished;
    // tract the game playing time
    private float timePlayed;
    // boolean to make sure the player is on the ground so the player can jump again
    private bool OnGround;
    void Start()
    {
        // set the game playing time to 0 and game is not finished at the beginning of the game
        // set the player rigidbody and set the count to 0 with countText
        timePlayed = 0;
        gameFinished = false;
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";

        // create a int to count down the pickups that spawn at the beginning of the game
        int pickupsSpawned = 20;
        while (pickupsSpawned > 0)
        {
            pickupsSpawned--;
            SpawnPickup();
        }

    }
    // randomly spawn pickup prefab within the platform of the game
    void SpawnPickup()
    {
        Vector3 randomPosition = new Vector3(Random.Range(-8f, 8f), .5f, Random.Range(-8f, 8f));
        Instantiate(pickupPrefab, randomPosition, pickupPrefab.transform.rotation);
    }


    private void Update()
    {

        // tract the speed of player. if it is 0, player lose the game
        if (speed == 0)
        {
            winText.text = "You Lost!";
            // make sure set the game is finished
            gameFinished = true;
        }

        // if the game is not finished, playing time should be updated
        if (gameFinished == false)
        {
            timePlayed += Time.deltaTime;
            print("Play Time: " + timePlayed);
        }
        // use raycast to check if the player is on ground or not
        OnGround = Physics.Raycast(transform.position, Vector3.down, .52f);

        // if Space key is pressed and the player is on the ground, player is able tp jump
        if (Input.GetKeyDown(KeyCode.Space) && OnGround)
        {
            // use the physics that rigibody has as gravity and use it in the opposite way to make the player jump
            GetComponent<Rigidbody>().AddForce(Vector3.up * 300);
        }

    }

    void FixedUpdate()
    {   
        // Player controller by horizontal and vertical
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        // set the direction
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        // set the speed
        rb.AddForce(movement * speed);
    }

    // collision check of player for all pickups 
    void OnTriggerEnter(Collider other)
    {

        // if the collider is pick up, added 1 to count
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
        }
        // if the collider is pick up(Debuff), reduce the speed of player by 2
        if (other.gameObject.CompareTag("Pick Up (Debuff)"))
        {
            other.gameObject.SetActive(false);
            // make sure the speed does not go to negative
            if (speed >= 2)
            {
                speed -= 2;
            }
        }
    }

    // collision check of player enemy
    private void OnCollisionEnter(Collision collision)
    {
     
        if (collision.gameObject.CompareTag("Enemy"))
        {
            winText.text = "You Lost!";
            // game is finished if the player is caught by enemy
            gameFinished = true;
            // delay 2s, then reset the game level
            Invoke("RestartLevel", 2f);
        }
    }

    // function to update CountText and WinText
    void SetCountText()
    {
        // print the count
        countText.text = "Count: " + count.ToString();
        // if count score is 20
        if (count >= 20)
        {
            // Set winText to "You Win!"
            winText.text = "You Win!";
            // game is finished
            gameFinished = true;

            // Delay 2s then restart the game
            Invoke("RestartLevel", 2f);
        }
    }

    void RestartLevel()
    {
        // reset the game level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


}
