﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        //rotate the pick up with a vector3
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}
