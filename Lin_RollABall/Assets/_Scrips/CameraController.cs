﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // public for reference the player gameobject
    public GameObject player;
    // new Vector3 for our camera
    Vector3 offset;  

    // Start is called before the first frame update
    void Start()
    {
        // Set the camera on the player at the beginning of the game
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // update the camera which follows the player on the top angle every frame
        transform.position = player.transform.position + offset;
    }
}
