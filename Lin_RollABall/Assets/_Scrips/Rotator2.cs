﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator2 : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        //rotate the pick up with a vector3 
        transform.Rotate(new Vector3(45, 30, 15) * Time.deltaTime);
    }
}
