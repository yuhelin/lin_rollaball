﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // player reference for enemy to chase
    public GameObject player;
    // rigidbody for player
    Rigidbody rb;
    // scale the speed of enemy
    public float speed;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        // use the position of the player.x - enemy.x and player.y - enemy.y to set the direction of the enemy that can chase after the player
        Vector3 movement = new Vector3(player.transform.position.x - Input.GetAxis("Horizontal"), 0.0f, player.transform.position.z - Input.GetAxis("Vertical"));
        // add the force by direction and speed
        rb.AddForce(movement * speed);
    }
}
